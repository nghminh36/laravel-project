<?php

namespace Database\Seeders;

use App\Models\Permisson;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Hiệu',
            'email' => 'a@gmail.com',
            'birthday' => '2000/4/9',
            'address' => 'my dinh - ha noi',
            'phone_number' => '0963634044',
            'gender' => '0',
            'password' => bcrypt(123456)
        ]);

        $role = Role::create([
            'name' => 'super_admin',
            'display_name' => 'SuperAdmin'
        ]);

        DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);

        $permissions = Permisson::all();

        foreach ($permissions as $permission) {
            DB::table('role_permission')->insert([
                'permission_id' => $permission->id,
                'role_id' => $role->id
            ]);
        }
    }
}

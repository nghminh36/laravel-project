<?php

namespace Database\Seeders;

use App\Models\Permisson;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Tùng',
            'email' => 'c@gmail.com',
            'birthday' => '1998/5/1',
            'address' => 'mai dich - ha noi',
            'phone_number' => '098147475',
            'gender' => '0',
            'password' => bcrypt(123456)
        ]);

        $role = Role::create([
            'name' => 'user',
            'display_name' => 'User'
        ]);

        DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);

        DB::table('role_permission')->insert([
            ['permission_id' => 15, 'role_id' => $role->id],
            ['permission_id' => 22, 'role_id' => $role->id]
        ]);
    }
}

@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Users Management</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">All Users</li>
                </ol>
                <div class="container-fluid px-4">
                    @hasPermission('user_create')
                    <a href="{{ route('users.create') }}" class="btn btn-success">Create New User</a>
                    @endhasPermission
                    <div class="row justify-content-center">
                        <form method="GET" class="col-md-5 m-md-3 d-flex">
                            <select name="role_id" class=" form-control col-md-2 mr-2"
                                    aria-label="Default select example">
                                <option value=" " selected>Roles</option>
                                @foreach($roles as $role)
                                    <option {{ $role->id == request('role_id') ? 'selected' : '' }}
                                            value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>

                            <input type="search" name="name" value="{{ request('name') }}" class="form-control"
                                   placeholder="search ...">
                            <button type="submit" class="btn btn-primary">
                                search
                            </button>
                        </form>
                    </div>

                    <div class="row justify-content-center">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Birthday</th>
                                <th scope="col">Address</th>
                                <th scope="col">Phone number</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Roles</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{$user->id}}</th>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->birthday}}</td>
                                    <td>{{$user->address}}</td>
                                    <td>{{$user->phone_number}}</td>
                                    <td>
                                        {{ $user->gender == 0 ? 'male' : 'female' }}
                                    </td>
                                    <td>
                                        @foreach($user->roles as $role)
                                            <span style="font-size: 12px"
                                                  class="badge badge-success">{{ $role->display_name }}</span>
                                        @endforeach
                                    </td>

                                    <td>
                                        @hasPermission('user_show')
                                        <a href="{{route('users.show',$user->id)}}" class="btn btn-primary"><i
                                                class="far fa-eye"></i></a>
                                        @endhasPermission
                                        @hasPermission('user_edit')
                                        <a href="{{route('users.edit',$user->id)}}" class="btn btn-success"><i
                                                class="far fa-edit"></i></a>
                                        @endhasPermission
                                        @hasPermission('user_delete')
                                        <button type="submit" class="btn btn-danger btn-delete"
                                                onclick="deleteUser({{$user->id}})"><i
                                                class="fas fa-trash-alt"></i></button>
                                        @endhasPermission
                                        <form action="{{ route('users.delete', $user->id)}}" id="form-{{$user->id}}"
                                              method="POST" style="display: none">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="container-fluid">
                            {{$users->links()}}
                        </div>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteUser(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        }
    </script>
@endpush



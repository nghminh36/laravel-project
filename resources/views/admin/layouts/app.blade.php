<!DOCTYPE html>
<html lang="en">
@include('admin.layouts.head')

<body class="sb-nav-fixed">
@include('admin.layouts.nav')

<div id="layoutSidenav">
    @include('admin.layouts.sitenav')

    @yield('content')

</div>

@include('admin.layouts.js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@stack('scripts')
</body>
</html>

@extends('admin.layouts.app')

@section('title', 'Categories')

@section('content')
    <!--Create Modal -->
    <div class="modal fade" id="CreateCategoryModal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Category</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form id="CreateCategoryForm" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <ul class="alert alert-warning d-none" id="_error_create"></ul>
                        <div class="form-group mb-3">
                            <label>Name</label>
                            <input type="text" class="form-control" id="nameCreate" name="name">
                            <span class="text-danger name_error_create"></span>
                        </div>
                        <div class="form-group mb-3">
                            <label>ParentID</label>
                            <select class="form-control" id="parentIdCreate" name="parent_id">
                                <option value="">Chọn danh mục</option>
                                @foreach($parentIds as $parentId)
                                    <option value="{{$parentId->id}}">{{$parentId->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-create"
                                data-action="{{route('categories.store')}}">Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Edit Modal -->
    <div class="modal fade" id="EditCategoryModal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form id="EditCategoryForm" enctype="multipart/form-data">
                    <div class="modal-body">
                        <ul class="alert alert-warning d-none" id="_error_update"></ul>
                        <div class="form-group mb-3">
                            <label>Name</label>
                            <input type="text" class="form-control" id="nameUpdate" name="name">
                            <span class="text-danger name_error_update"></span>
                        </div>
                        <div class="form-group mb-3">
                            <label>ParentID</label>
                            <select class="form-control" id="parentIdUpdate" name="parent_id">
                                <option value="">Chọn danh mục</option>
                                @foreach($parentIds as $parentId)
                                    <option value="{{$parentId->id}}">{{$parentId->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-update">Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">List Categories</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">All Category</li>
                </ol>
                <div class="container-fluid px-4">
                    @hasPermission('category_create')
                    <a href="#" data-bs-toggle="modal" data-bs-target="#CreateCategoryModal" class="btn btn-success">Create
                        Category</a>
                    @endhasPermission
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <form method="GET" class="m-md-3 d-flex">
                                <input type="search" name="name" id="name" value="{{ request('name') }}"
                                       class="form-control"
                                       placeholder="search ...">
                            </form>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div id="success_message">
                        </div>
                        <div id="list" data-action="{{route('categories.list')}}">
                        </div>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('admin/js/category.js') }}"></script>
@endpush


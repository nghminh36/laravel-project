<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    protected UserService $userService;

    protected RoleService $roleService;

    protected ProductService $productService;

    protected CategoryService $categoryService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        UserService $userService,
        RoleService $roleService,
        ProductService $productService,
        CategoryService $categoryService
    ) {
        $this->middleware('auth');
        $this->userService = $userService;
        $this->roleService = $roleService;
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $users = $this->userService->count();

        $roles = $this->roleService->count();

        $products = $this->productService->count();

        $categories = $this->categoryService->count();

        return view('admin.home.dashboard', compact('users', 'roles', 'products', 'categories'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{
    protected CategoryService $categoryService;

    protected ProductService $productService;

    /**
     * @param $product
     */
    public function __construct(CategoryService $categoryService, ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->productService = $productService;
        View::share('categories', $this->categoryService->getAll());
    }

    public function index(Request $request)
    {
        $products = $this->productService->search($request);

        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(CreateProductRequest $request)
    {
        $this->productService->create($request);

        return redirect()->route('products.index');
    }

    public function show($id)
    {
        $product = $this->productService->getById($id);

        return view('admin.products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = $this->productService->getById($id);

        return view('admin.products.edit', compact('product'));
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $this->productService->update($request, $id);

        return redirect()->route('products.index');
    }

    public function destroy($id)
    {
        $this->productService->delete($id);

        return redirect()->route('products.index');
    }
}

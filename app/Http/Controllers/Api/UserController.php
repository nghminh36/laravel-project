<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    protected $user;

    /**
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->paginate(5);
        $usersCollection = new UserCollection($users);
        return response()->json([
            'data' => $usersCollection,

        ], Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $dataCreate = $request->all();
        $user = User::create($dataCreate);
        $postResource = new UserResource($user);
        return $this->sendResponse($postResource, "Thêm thành công");
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $postResource = new UserResource($user);
        return $this->sendResponse($postResource, 'Yêu cầu thành công');
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $dataUpdate = $request->all();
        $user->update($dataUpdate);
        return $this->sendResponse(new UserResource($user), 'Cập nhật thành công');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $this->sendResponse(new UserResource($user), 'Xóa thành công!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateNewCategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    protected CategoryService $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $categories = $this->categoryService->search($request);

        $parentIds = $this->categoryService->getParentIds();
        return view('admin.categories.index', compact('categories', 'parentIds'));
    }

    public function list(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('admin.categories.list', compact('categories'))->render();
    }

    public function create()
    {
        return view('admin.categories.index', ['categories'=>$this->categoryService->getParent()]);
    }

    public function store(CreateNewCategoryRequest $request)
    {
        $this->categoryService->store($request);
        return response()->json([
            'status' => Response::HTTP_OK,
            'message' => 'Create Category Successfully!'
        ]);
    }

    public function edit($id)
    {
        $category = $this->categoryService->findById($id);
        return response()->json([
            'status' => Response::HTTP_OK,
            'category'=> $category
        ]);
    }

    public function update(CreateNewCategoryRequest $request, $id)
    {
        $this->categoryService->update($request, $id);
        return response()->json([
            'status' => Response::HTTP_OK,
            'message' => 'Update Category Success !!!',
        ]);
    }

    public function destroy($id)
    {
        $this->categoryService->delete($id);
        return response()->json([
            'status' => Response::HTTP_OK,
            'message'=>'Data delete succesfully!'
        ]);
    }
}

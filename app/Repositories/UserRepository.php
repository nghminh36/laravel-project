<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function search($dataSearch)
    {
        $name = $dataSearch['name'];
        $email = $dataSearch['email'];
        $roleId = $dataSearch['role_id'];
        return $this->model->withNameEmail($name, $email)->withRoleId($roleId)->latest('id')->paginate(5);
    }

    public function getRoleId($id)
    {
        return $this->model->find($id)->getUserRole($id);
    }
}

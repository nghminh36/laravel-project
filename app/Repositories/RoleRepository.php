<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }


    public function search($dataSearch)
    {
        $name = $dataSearch['name'];

        return $this->model->withName($name)->latest('id')->paginate(10);
    }

    public function getIdPermission($id)
    {
        return $this->model->find($id)->getIdPermission($id);
    }

    public function withoutSuperAdmin()
    {
        return $this->model->withoutSuperAdmin()->get();
    }
}

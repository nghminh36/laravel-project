<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'birthday',
        'address',
        'phone_number',
        'gender',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function scopeWithNameEmail($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%')
            ->orWhere('email', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithRoleId($query, $roleId)
    {
        return $roleId ? $query->WhereHas('roles', function ($q) use ($roleId) {
            $q->where('roles.id', $roleId);
        }) : null;
    }

    public function syncRole($roleId)
    {
        return $this->roles()->sync($roleId);
    }

    public function attachRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function detachRole()
    {
        return $this->roles()->detach();
    }
    // Chuyen qua repo
    public function getUserRole($id)
    {
        return $this->roles()->where('user_id', $id)->pluck('role_id');
    }

    public function hasRole($role)
    {
//        if ($this->roles->contains('name', $role)) {
//            return true ;
//        }
//        return false;

        return $this->roles->contains('name', $role);
    }

    public function hasPermission($permission)
    {
        foreach ($this->roles as $role) {
            if ($role->permissions->contains('name', $permission)) {
                return true;
            }
        }
        return false;
    }

    public function isSuperAdmin()
    {
//        return $this->roles->contains('name', 'super_admin');
        return $this->hasRole('super_admin');
    }
}

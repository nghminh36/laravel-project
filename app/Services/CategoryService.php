<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\CategoryRepository;

class CategoryService
{
    protected CategoryRepository $categoryRepository;

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getParent()
    {
        return Category::where('parent_id', 0)->get();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        return $this->categoryRepository->search($dataSearch)->appends($request->all());
    }

    public function getAll()
    {
        return $this->categoryRepository->getAll();
    }

    public function getCategoryGroup()
    {
        return $this->categoryRepository->getCategoryGroup();
    }

    public function store($request)
    {
        $categoryCreate = $request->all();

        return $this->categoryRepository->create($categoryCreate);
    }

    public function delete($id)
    {
        $category = $this->categoryRepository->find($id);
        $category->delete();
        return $category;
    }


    public function findById($id)
    {
        return $this->categoryRepository->find($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $category = $this->categoryRepository->find($id);
        $category->update($dataUpdate);
        return $category;
    }

    public function getParentIds()
    {
        return $this->categoryRepository->getParentId();
    }

    public function count()
    {
        return $this->categoryRepository->count();
    }
}

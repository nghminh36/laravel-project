<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected RoleRepository $roleRepository;

    /**
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }


    public function getAllRole($request)
    {
        $dataSearch = $request->all();

        $dataSearch['name'] = $request->name ?? '';

        return $this->roleRepository->search($dataSearch)->appends($request->all());
    }

    public function getAll()
    {
        return $this->roleRepository->getAll();
    }

    public function create($request)
    {
        $dataCreate = $request->all();

        $role = $this->roleRepository->create($dataCreate);

        $role->attachPermission($request->display_name_permission);

        return $role;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();

        $role = $this->roleRepository->find($id);

        $role->update($dataUpdate);

        $role->syncPermission($request->display_name_permission);

        return $role;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->find($id);
        $role->detachPermission();
        $role->detachUser();
        $role->delete();
        return $role;
    }

    public function getPermissionIdRole($id)
    {
        return $this->roleRepository->getIdPermission($id);
    }

    public function getRoleId($id)
    {
        return $this->roleRepository->find($id);
    }

    public function withoutSuperAdmin()
    {
        return $this->roleRepository->withoutSuperAdmin();
    }

    public function count()
    {
        return $this->roleRepository->count();
    }
}

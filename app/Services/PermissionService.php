<?php

namespace App\Services;

use App\Repositories\BaseRepository;
use App\Repositories\PermissionRepository;

class PermissionService
{
    protected PermissionRepository $permissionRepository;

    /**
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getAll()
    {
        return $this->permissionRepository->getAll();
    }

    public function getWithGroup()
    {
        return $this->permissionRepository->getWithGroup();
    }
}

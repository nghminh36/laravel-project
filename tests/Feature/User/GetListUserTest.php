<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListUserTest extends TestCase
{
    /** @test */
    public function super_admin_get_all_users()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get(route('users.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.index');
        $response->assertSee($user->name)->assertSee($user->email);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_all_users()
    {
        $this->loginUserWithPermission('user_show');
        $user = User::factory()->create();
        $response = $this->get(route('users.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.index');
        $response->assertSee($user->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_all_users()
    {
        $user = User::factory()->create();
        $response = $this->get(route('users.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}

<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_have_permission_can_update_category()
    {
        $this->withoutExceptionHandling();
        $category = Category::factory()->create();
        $this->loginUserWithPermission('category_edit');
        $updateData = [
            'name' => 'admin',
            'phone' => '0123456789',
        ];
        $response = $this->json('PUT', '/categories/' . $category->id, $updateData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_OK)
                ->where('message', 'Update Category Success !!!')
                ->etc()
            );
    }

    /** @test */
    public function authenticated_super_admin_can_update_category()
    {
        $category = Category::factory()->create();
        $this->loginWithSuperAdmin();
        $updateData = [
            'name' => 'admin',
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', '/categories/' . $category->id, $updateData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_OK)
                ->where('message', 'Update Category Success !!!')
                ->etc()
            );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $category = Category::factory()->create();
        $updateData = [
            'name' => 'admin',
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', '/categories/' . $category->id, $updateData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_update_category()
    {
        $category = Category::factory()->create();
        $updateData = [
            'name' => 'admin',
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', '/categories/' . $category->id, $updateData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function user_have_permission_can_not_update_user_account_if_name_is_null()
    {
        $category = Category::factory()->create();
        $this->loginUserWithPermission('category_edit');
        $updateData = [
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', '/categories/' . $category->id, $updateData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function super_admin_can_not_update_user_account_if_name_is_null()
    {
        $category = Category::factory()->create();
        $this->loginWithSuperAdmin();
        $updateData = [
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', '/categories/' . $category->id, $updateData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}

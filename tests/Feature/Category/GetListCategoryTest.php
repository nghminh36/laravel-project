<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_can_get_all_category()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.index');
    }

    /** @test */
    public function authenticated_user_can_get_list_category()
    {
        $this->loginUserWithPermission('category_view');
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.categories.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

}

<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_get_all_roles()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get(route('roles.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_all_roles()
    {
        $this->loginUserWithPermission('role_view');
        $role = Role::factory()->create();
        $response = $this->get(route('roles.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_all_roles()
    {
        $role = Role::factory()->create();
        $response = $this->get(route('roles.index'));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($role->display_name);
    }
}

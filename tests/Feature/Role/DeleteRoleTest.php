<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_delete_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.delete', $role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_delete_role()
    {
        $this->loginUserWithPermission('role_delete');
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.delete', $role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_user_can_not_delete_role()
    {
        $this->loginWithUser();
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.delete', $role->id));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('roles', $role->toArray());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_role()
    {
        $role = Role::factory()->create();
        $response = $this->delete(route('roles.delete', $role->id));
        $this->assertDatabaseHas('roles', $role->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}

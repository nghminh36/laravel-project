<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function super_admin_can_get_all_products()
    {
        $this->loginWithSuperAdmin();

        $role = Product::factory()->create();

        $response = $this->get(route('products.index'));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('admin.products.index');

        $response->assertSee($role->display_name);
    }

    /** @test */
    public function super_admin_can_get_single_product()
    {
        $this->loginWithSuperAdmin();

        $role = Product::factory()->create();

        $response = $this->get(route('products.show', $role->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('admin.products.show');

        $response->assertSee($role->display_name);
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_get_all_products()
    {
        $this->loginWithUser();

        $response = $this->get(route('users.create'));

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_get_single_product()
    {
        $this->loginWithUser();

        $product = Product::factory()->create();

        $response = $this->get(route('products.show', $product->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertDontSee($product->name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_all_products()
    {
        $this->loginUserWithPermission('product_view');

        $product = Product::factory()->create();

        $response = $this->get(route('products.index'));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('admin.products.index');

        $response->assertSee($product->name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_single_product()
    {
        $this->loginUserWithPermission('product_view');

        $product = Product::factory()->create();

        $response = $this->get(route('products.show', $product->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('admin.products.show');

        $response->assertSee($product->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_all_products()
    {
        $product = Product::factory()->create();

        $response = $this->get(route('products.index'));

        $response->assertRedirect('/login');

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertDontSee($product->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_single_product()
    {
        $product = Product::factory()->create();

        $response = $this->get(route('products.show', $product->id));

        $response->assertRedirect('/login');

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertDontSee($product->name);
    }
}

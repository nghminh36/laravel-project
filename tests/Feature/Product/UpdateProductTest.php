<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    use WithFaker;

    public function __getDataValidate($data = [])
    {
        return array_merge([
            'name' => $this->faker->name,
            'price' => '10000',
            'description' => $this->faker->text,
            'category_id' => Category::factory()->create()->id,
        ], $data);
    }

    /** @test */
    public function authenticated_super_admin_can_see_edit_product_form()
    {
        $this->loginWithSuperAdmin();

        $product = Product::factory()->create();

        $response = $this->get(route('products.edit', $product->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('admin.products.edit');

        $response->assertSee($product->name)->assertSee($product->price);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_product_view()
    {
        $product = Product::factory()->create();

        $response = $this->get(route('products.edit', $product->id));

        $response->assertRedirect('/login');

        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_super_admin_can_update_product()
    {
        $this->loginWithSuperAdmin();

        $product = Product::factory()->create();

        $data = $this->__getDataValidate();

        $response = $this->put(route('products.update', $product->id), $data);

        $this->assertDatabaseHas('products', ['name' => $data['name']]);

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertRedirect(route('products.index'));
    }

    /** @test */
    public function authenticated_super_admin_can_not_update_product_name_is_null()
    {
        $this->loginWithSuperAdmin();

        $product = Product::factory()->create();

        $data = ['name' => null, 'price' => null];

        $response = $this->put(route('products.update', $product), $data);

        $response->assertSessionHasErrors('name')->assertSessionHasErrors('price');
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_edit_product_form()
    {
        $this->loginUserWithPermission('product_edit');

        $product = Product::factory()->create();

        $response = $this->get(route('products.edit', $product->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('admin.products.edit');

        $response->assertSee('name')->assertSee('price');
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_name_if_price_are_null()
    {
        $this->loginUserWithPermission('product_edit');

        $product = Product::factory()->create();

        $data = [
            'name' => null,
            'price' => null
        ];

        $response = $this->put(route('products.update', $product->id), $data);

        $response->assertSessionHasErrors(['name', 'price']);

        $this->assertDatabaseMissing('products', $data);
    }
}

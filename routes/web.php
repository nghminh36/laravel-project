<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::middleware(['auth'])->group(function () {
    Route::name('users.')->prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])
            ->name('index')
            ->middleware('permission:user_view');

        Route::get('/edit/{id}', [UserController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:user_edit');

        Route::get('/create', [UserController::class, 'create'])
            ->name('create')
            ->middleware('permission:user_create');

        Route::post('/', [UserController::class, 'store'])
            ->name('store')
            ->middleware('permission:user_store');

        Route::delete('/{user}', [UserController::class, 'destroy'])
            ->name('delete')
            ->middleware(['permission:user_delete', 'check.user']);

        Route::put('/{id}', [UserController::class, 'update'])
            ->name('update')
            ->middleware('permission:user_update');

        Route::get('users/{id}', [UserController::class, 'show'])
            ->name('show')
            ->middleware('permission:user_show');
    });

    Route::name('roles.')->prefix('roles')->group(function () {
        Route::get('/', [RoleController::class, 'index'])
            ->name('index')
            ->middleware('permission:role_view');

        Route::get('/edit/{id}', [RoleController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:role_edit');

        Route::get('/create', [RoleController::class, 'create'])
            ->name('create')
            ->middleware('permission:role_create');

        Route::post('/', [RoleController::class, 'store'])
            ->name('store')
            ->middleware('permission:role_store');

        Route::delete('/delete/{id}', [RoleController::class, 'destroy'])
            ->name('delete')
            ->middleware('permission:role_delete');

        Route::put('/{id}', [RoleController::class, 'update'])
            ->name('update')
            ->middleware('permission:role_update');

        Route::get('users/{id}', [RoleController::class, 'show'])
            ->name('show')
            ->middleware('permission:role_show');
    });

    Route::name('products.')->prefix('products')->group(function () {
        Route::get('/', [ProductController::class, 'index'])
            ->name('index')
            ->middleware('permission:product_view');

        Route::get('/create', [ProductController::class, 'create'])
            ->name('create')
            ->middleware('permission:product_create');

        Route::get('/edit/{id}', [ProductController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:product_edit');

        Route::put('/{id}', [ProductController::class, 'update'])
            ->name('update')
            ->middleware('permission:product_update');

        Route::post('/', [ProductController::class, 'store'])
            ->name('store')
            ->middleware('permission:product_store');

        Route::delete('/delete/{id}', [ProductController::class, 'destroy'])
            ->name('delete')
            ->middleware('permission:product_delete');

        Route::get('/{id}', [ProductController::class, 'show'])
            ->name('show')
            ->middleware('permission:product_show');
    });

    Route::name('categories.')->prefix('categories')->group(function () {
        Route::get('/', [CategoryController::class, 'index'])
            ->name('index')
            ->middleware('permission:category_view');

        Route::get('/create', [CategoryController::class, 'create'])
            ->name('create')
            ->middleware('permission:category_create');

        Route::post('/', [CategoryController::class, 'store'])
            ->name('store')
            ->middleware('permission:category_store');

        Route::get('/list', [CategoryController::class, 'list'])
            ->name('list')
            ->middleware('permission:category_view');

        Route::get('/edit/{id}', [CategoryController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:category_edit');

        Route::put('/{category}', [CategoryController::class, 'update'])
            ->name('update')
            ->middleware('permission:category_update');

        Route::delete('/{category}', [CategoryController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:category_delete');
    });
});


